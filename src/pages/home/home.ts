import { Component } from '@angular/core';
import {NavController, Alert, AlertController, reorderArray} from 'ionic-angular';
import {TodoProvider} from '../../providers/todo/todo';
import { ArchivedTodosPage } from '../../pages/archived-todos/archived-todos';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public todos = [];
  public reorderIsEnabled = true;
  constructor(private todoService: TodoProvider, public navCtrl: NavController, private alertController: AlertController) {
    this.todos = this.todoService.getTodos();


  }

    goToArchivePage(){

      this.navCtrl.push(ArchivedTodosPage);
    }
    ionItemReordered($event){
      console.log($event);
      reorderArray(this.todos, $event);
    }
    toogleReorder(){
      this.reorderIsEnabled = !this.reorderIsEnabled;
    }
    openTodoAlert(){
      let addTodoAlert = this.alertController.create({
        title: "Add a Todo",
          message: "Enter your todo",
          inputs: [
              {
                type: "text",
                  name: "addTodoInput"
              }
          ],
          buttons: [
              {
                text: "Cancel",
              },
              {
                text: "Add Todo",
                  handler: (inputData) => {
                    let todoText;
                    todoText = inputData.addTodoInput;
                    //this.todos.push(todoText);
                    this.todoService.addTodo(todoText);
                  }
              }
          ]
      });
      addTodoAlert.present();
    }
}
